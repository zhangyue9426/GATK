%define debug_package %{nil}

Name:     gatk
Version:  4.2.0.0
Release:  1
Summary:  GATK - Genome Analysis Toolkit
License:  MIT
URL:      https://github.com/broadinstitute/gatk


Source0:   gatk-4.2.0.0.tar.gz.0
Source1:   gatk-4.2.0.0.tar.gz.1
Source2:   gatk-4.2.0.0.tar.gz.2
Source3:   gatk-4.2.0.0.tar.gz.3
Source4:   gatk-4.2.0.0.tar.gz.4
Source5:   gatk-4.2.0.0.tar.gz
Source6:   LICENSE

%description

The Genome Analysis Toolkit or GATK is a software package developed at
the Broad Institute to analyze high-throughput sequencing data. The
toolkit offers a wide variety of tools, with a primary focus on variant
discovery and genotyping as well as strong emphasis on data quality
assurance. Its robust architecture, powerful processing engine and
high-performance computing features make it capable of taking on
projects of any size.


%prep
cat %SOURCE0 %SOURCE1 %SOURCE2 %SOURCE3 %SOURCE4  >  %{_sourcedir}/gatk-4.2.0.0.tar.gz


%setup  -T -b 5
%{__cp} -a %SOURCE6 LICENSE


%build
install  -d  %{buildroot}%{_bindir}/
install -d -p -m 0755 %{buildroot}%{_datadir}/
%{__cp} -a %{_builddir}/%{name}-%{version}/*  %{buildroot}%{_datadir}/
ln -sf /usr/share/%{name}-%{version}/gatk %{buildroot}%{_bindir}/


%files
%{_bindir}/%{name}
%{_datadir}/%{name}-%{version}
%dir %{_datadir}/%{name}-%{version}
%doc %{_datadir}/LICENSE


%changelog
* Sat May 15 2021 duyiwei <duyiwei@kylinos.cn> - 4.2.0.0-1
- Package init